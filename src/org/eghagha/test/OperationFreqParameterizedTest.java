package org.eghagha.test;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.eghagha.csce.Context;
import org.eghagha.csce.OperationFreq;
import org.eghagha.csce.OperationGrep;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;


@RunWith(Parameterized.class)
public class OperationFreqParameterizedTest {
	
	OperationFreq og;
	
	
    // fields used together with @Parameter must be public
    @Parameter(0)
    public String s1;
    @Parameter(1)
    public String result;
    @Parameter(2)
    public boolean b1;
    
    
    // creates the test data
    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { "1by1.txt", "{1=1}", true }, { "5by5.txt", "{1=3, 2=4, 3=4, t=1, 4=4, e=1, 5=5, i=1, j=2}", true }, { "0by0.txt", "{0=0}", true } };
        return Arrays.asList(data);
    }
    
	
	@Test
	public void testFreq() {
		System.out.println("Testing frequency in " + s1);
		og = new OperationFreq(s1);
		og.doOperation();
		assertEquals("Result", result, og.toString());
		assertEquals("True if there is at least one word", b1 , og.isThereWordsArray()); 
	      
	}
	

}
