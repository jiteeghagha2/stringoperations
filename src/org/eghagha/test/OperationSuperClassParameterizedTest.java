package org.eghagha.test;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collection;

import org.eghagha.csce.OperationSuperClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class OperationSuperClassParameterizedTest {

	@Rule
	public final ExpectedException exception = ExpectedException.none();
	  
	OperationSuperClass og;
	
    // fields used together with @Parameter must be public
    @Parameter(0)
    public String s1;
    @Parameter(1)
    public boolean b1;
    @Parameter(2)
    public String s2;
    
    // creates the test data
    @Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][] { { "10by10.txt", true, "10_100" }, { "1by1.txt", true, "1_1" }, { "0by0.txt", true, "0_0" } };
        return Arrays.asList(data);
    }

	@Test
	public void readFileTest() {
		
	  System.out.println("Testing read from a " + s1 + " file\n");
	  og = new OperationSuperClass(s1);
	  assertEquals("True if number of line is > 0", b1, og.isThereLineArray());
	  assertEquals("True if wordsArray > 0", b1 , og.isThereWordsArray());  
	  assertEquals("size of data structs",s2 , og.toString());  
	  System.out.println(s1 + " file has at least one word, and one line\n");

	      
	}


}
