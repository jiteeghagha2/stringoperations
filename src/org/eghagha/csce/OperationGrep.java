package org.eghagha.csce;

public class OperationGrep extends OperationSuperClass implements Strategy {

	private int line_count = 0;
	private String word = "";
	
	public OperationGrep(String word, String address) {
		super(address);
		this.word = word;
	}
	 public void OperationGrep(String input){
			this.word = input;

	 }
	   public void doOperation(String data) 
	   {
		   try {
			   if(data.contains(word)) {
				   System.out.print(data);
			   }
		   }
		   catch(Exception e)
			{
				e.printStackTrace();
			}
	        
	   }
	@Override
	public void doOperation() {
		/*
		 * one loop for iterating on words in a line.
		 * check for the word in the line and print a message if found
		 */
		for (int x = 0; x < lineArray.size(); x++) {
			// check for the word in the line and print a message if found
			String line = lineArray.get(x);
			if (line.contains((word))) {
				setLine_count(getLine_count() + 1);
				System.out.println(line);
			}
		}
	}

	public int getLine_count() {
		return line_count;
	}

	private void setLine_count(int line_count) {
		this.line_count = line_count;
	}
	
	public String toString() {
		return "";
	}
	
	//can use this method to test if doOperation got a word to operate on
	public boolean isThereLine(){
		return (lineArray.size() > 0);
	}
}
