package org.eghagha.csce;

/*

There are 3 operations - word Count, Grep, and Frequency
 	
The Strategy pattern suggests keeping the implementation of each of the operations in a separate class. 

Each operation encapsulated in a separate class is referred to as a strategy.

An object that uses a Strategy object is often referred to as a Context object.

To enable a Context object to access different Strategy objects in a seamless manner, all Strategy objects must be designed to implement the Strategy interface.

This StrategyPatten application can choose from among these operation by selecting and instantiating an appropriate Strategy class.

All Strategy objects extend their SuperClass which uses a single while loop to read the file.

Strategy objects are unaware of how the SuperClass reads a file. 

 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class StrategyPattern {

	public static void main(String[] args) {

		BufferedReader br = null;
		Context context = null;
		boolean doOperation = true;
		try {

			br = new BufferedReader(new InputStreamReader(System.in));

			while (true) {

				System.out.print(">");

				StringTokenizer tokens = new StringTokenizer(br.readLine());
				// first token should be a command
				if (tokens.hasMoreTokens() && (tokens.countTokens() > 1 && tokens.countTokens() < 4) ) {
					String command = tokens.nextToken();
					
					context = new Context();

					if (command.equals("wc") & tokens.countTokens() == 1) {
						// second token should be a file location

						context.setStrategy(new OperationWc(tokens.nextToken()));

					} else if (command.equals("freq") & tokens.countTokens() == 1) {
						// second token should be a file location

						context.setStrategy(new OperationFreq(tokens.nextToken()));

					} else if (command.equals("grep") & tokens.countTokens() == 2) {
						// second and third tokens should be a search string and a file location respectively

						context.setStrategy(new OperationGrep(tokens.nextToken(), tokens.nextToken()));

					} else {
						// there must be an invalid argument(s)
						System.out.println("Invalid arguments error. Vaild commands : wc file.txt,  freq file.txt, grep searchString file.txt");
						doOperation = false;
					}
					
					if(doOperation)
					context.executeStrategy();

				}

			}

		} catch (IOException e) {
			System.out.println("test "+e.toString());
		}catch (NoSuchElementException e) {
			System.out.println("test "+e.toString());
		}  finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

}
